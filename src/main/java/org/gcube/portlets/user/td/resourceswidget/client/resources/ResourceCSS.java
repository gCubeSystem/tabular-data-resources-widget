/**
 * 
 */
package org.gcube.portlets.user.td.resourceswidget.client.resources;

import com.google.gwt.resources.client.CssResource;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public interface ResourceCSS extends CssResource {
	
	 @ClassName("cursor-zoom-in")
	 public String getCursorZoomIn(); 

	 @ClassName("cursor-zoom-out")
	 public String getCursorZoomOut(); 
	
}
